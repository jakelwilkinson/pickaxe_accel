#include "Adafruit_Sensor.h"
#include "Adafruit_ADXL345_U.h"

/* Assign a unique ID to this sensor at the same time */
Adafruit_ADXL345_Unified accel = Adafruit_ADXL345_Unified(12345);

int whipThreshold = 40;
int whipTimeout = 200;
long whipStart = -1000;
long lastWhip = -1000;
bool isWhipping = false;

struct Inputs
{
  int input_thumb_x = 0;
  int input_thumb_y = 0;
  int input_button_A = 0;
  int input_button_B = 0;
  float x_accel = 0;
  float y_accel = 0;
  float z_accel = 0;
  float roll = 0;
  float pitch = 0;
};

Inputs currentInputs;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);

  accel.begin();

  accel.setRange(ADXL345_RANGE_16_G);

}

void loop() {
  // put your main code here, to run repeatedly:
  UpdateInputValues();

  if (currentInputs.y_accel > whipThreshold){
    lastWhip = millis();
    isWhipping = true;
  }

  if (millis() - lastWhip > whipTimeout){
    isWhipping = false;
  }

  Serial.print(currentInputs.x_accel);
  Serial.print("\t");
  Serial.print(currentInputs.y_accel);
  
  Serial.print("\t");
  Serial.print(currentInputs.z_accel);
  Serial.print("\t");
  Serial.println(isWhipping * 120);
  delay(10);
}

void UpdateInputValues()
{
  sensors_event_t event;
  accel.getEvent(&event);

  currentInputs.x_accel = event.acceleration.z;
  currentInputs.y_accel = event.acceleration.x;
  currentInputs.z_accel = event.acceleration.y;

  currentInputs.pitch = atan2(currentInputs.y_accel, currentInputs.z_accel) * 57.3;
  currentInputs.roll = atan2((-currentInputs.x_accel), sqrt(currentInputs.y_accel * currentInputs.y_accel + currentInputs.z_accel * currentInputs.z_accel)) * 57.3;


}
